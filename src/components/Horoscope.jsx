import React from 'react';
import { Col } from 'react-bootstrap';


import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";


// Importo imagenes
import hor1 from '../img/horoscope/hor1.png';
import hor2 from '../img/horoscope/hor2.png';
import hor3 from '../img/horoscope/hor3.png';
import hor4 from '../img/horoscope/hor4.png';
import hor5 from '../img/horoscope/hor5.png';
import hor6 from '../img/horoscope/hor6.png';
import hor7 from '../img/horoscope/hor7.png';
import hor8 from '../img/horoscope/hor8.png';
import hor9 from '../img/horoscope/hor9.png';
import hor10 from '../img/horoscope/hor10.png';
import hor11 from '../img/horoscope/hor11.png';
import hor12 from '../img/horoscope/hor12.png';

// Importo flechas

import rightArrow from '../img/arrows/right-arrow.png'
import leftArrow from '../img/arrows/left-arrow.png'

// Importo CSS

import '../css/Horoscope.css';




export default class HoroscopeCarousel extends React.Component {


    state = {
        currentIndex: 0,
        responsive: { 1024: { items: 3 } },
    }

    slideTo = (i) => this.setState({ currentIndex: i })

    onSlideChanged = (e) => this.setState({ currentIndex: e.item })

    slideNext = () => this.setState({ currentIndex: this.state.currentIndex + 1 })

    slidePrev = () => this.setState({ currentIndex: this.state.currentIndex - 1 })


    render() {
        let hideHoroscope = ''
        if (this.props.state.horoscopeVisible === false) {
            hideHoroscope = 'hideHoroscope';
        }
        const { galleryItems, responsive, currentIndex } = this.state
        return (
            <Col md={12} className={hideHoroscope}>
                <Col className='app-choose'>
                    Choose your horoscope
                    </Col>

                <AliceCarousel buttonsDisabled={true} responsive={this.responsive} 
                responsive={responsive}
                slideToIndex={currentIndex}
                onSlideChanged={this.onSlideChanged} mouseDragEnabled dotsDisabled={true} swipeDisabled={false}>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Aquarius')}><img alt='' className='icon-app' src={hor1} /></div>
                        </div>
                        <h6 className='horoscope-name' >Aquarius</h6>
                        <p className='horoscope-dates'>January 20 - February 18</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Pisces')}><img alt='' className='icon-app' src={hor2} /></div>
                        </div>
                        <h6 className='horoscope-name'>Pisces</h6>
                        <p className='horoscope-dates'>February 19 - March 20</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Aries')}><img alt='' className='icon-app' src={hor3} /></div>
                        </div>
                        <h6 className='horoscope-name'>Aries</h6>
                        <p className='horoscope-dates'>March 21 - April 19</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Taurus')}><img alt='' className='icon-app' src={hor4} /></div>
                        </div>
                        <h6 className='horoscope-name'>Taurus</h6>
                        <p className='horoscope-dates'>April 20 - May 20</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Gemini')}><img alt='' className='icon-app' src={hor5} /></div>
                        </div>
                        <h6 className='horoscope-name'>Gemini</h6>
                        <p className='horoscope-dates'>May 21 - June 20</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Cancer')}><img alt='' className='icon-app' src={hor6} /></div>
                        </div>
                        <h6 className='horoscope-name'>Cancer</h6>
                        <p className='horoscope-dates'>June 21 - July 22</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Leo')}><img alt='' className='icon-app' src={hor7} /></div>
                        </div>
                        <h6 className='horoscope-name'>Leo</h6>
                        <p className='horoscope-dates'>July 23- August 22</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Virgo')}><img alt='' className='icon-app' src={hor8} /></div>
                        </div>
                        <h6 className='horoscope-name'>Virgo</h6>
                        <p className='horoscope-dates'>August 23 - September 22</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Libra')}><img alt='' className='icon-app' src={hor9} /></div>
                        </div>
                        <h6 className='horoscope-name'>Libra</h6>
                        <p className='horoscope-dates'>Septembre 23 - October 22</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Scorpio')}><img alt='' className='icon-app' src={hor10} /></div>
                        </div>
                        <h6 className='horoscope-name'>Scorpio</h6>
                        <p className='horoscope-dates'>October 23 - November 21</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Sagittarius')}><img alt='' className='icon-app' src={hor11} /></div>
                        </div>
                        <h6 className='horoscope-name'>Sagittarius</h6>
                        <p className='horoscope-dates'>November 22 - December 21</p>
                    </div>

                    <div className='image-container'>
                        <div className='image'>
                            <div onClick={() => this.props.horoscopeClick('Capricorn')}><img alt='' className='icon-app' src={hor12} /></div>
                        </div>
                        <h6 className='horoscope-name'>Capricorn</h6>
                        <p className='horoscope-dates'>December 22 - January 19</p>
                    </div>

                </AliceCarousel>
                <Col md={12} >
                <Col md={5} style={{margin: 'auto', textAlign: 'center'}} className="carousel-buttons">
                <div onClick={() => this.slidePrev()}><i className="fas fa-angle-left"></i></div>
                <div className="middle-dot"><i class="far fa-dot-circle"></i></div>
                <div onClick={() => this.slideNext()}><i className="fas fa-angle-right"></i></div>
                </Col>
                </Col>
            </Col>
        );
    }

}