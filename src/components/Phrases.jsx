import React, { Component } from 'react';
import { Col} from 'react-bootstrap';

import Rate from './Rate'

import '../css/Phrases.css';

const SERVER = 'http://localhost:3001/API';
const MODEL = '/phrases';
const RANDOM = '/random';




export default class Phrases extends Component {
    constructor(props) {
        super(props);
        this.state = { data: {} }
        this.getRandom = this.getRandom.bind(this);
        this.getRandom();
    }

    getRandom() {
        const fetchURL = `${SERVER}${MODEL}${RANDOM}`;
        //console.log("Requesting random from: " + fetchURL);
        return new Promise((resolve, reject) => {
            fetch(fetchURL)
                .then(results => results.json())
                .then(phrase =>{
                     this.setState({ data: phrase.data[0]})
                    console.log("estado " + this.state.data.id)
                } )
                .catch(err => reject([{ error: err }]));
        })


    }

    setRating(ratingValue) {
        //let currentRating = this.state.data.puntuacionTotal;

        const fetchURL = `${SERVER}${MODEL}${RANDOM}`;
        //console.log("Requesting random from: " + fetchURL);
        return new Promise((resolve, reject) => {
            fetch(fetchURL)
                .then(results => results.json())
                .then(phrase => this.setState({ data: phrase.data[0] }))    
                .catch(err => reject([{ error: err }]));
        })
    }

    render() {
        if (this.state.data.id) {
            return (
                <Col md={12} className="phrases-container">
                <Col>
                    <h1>{this.state.data.frase}</h1>
                    <h2>{this.state.data.autor}</h2>
                </Col>
                <Col md={12} className="phrases-rating">
                    <Rate data={this.state.data}></Rate>
                </Col>
                </Col>
            )
        } else {
            return (
                <h1>Phrase not found</h1>
            )
        }

    }







}