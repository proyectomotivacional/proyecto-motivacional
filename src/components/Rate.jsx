import React from 'react';

import star from '../img/stars/star.png';
import starYellow from '../img/stars/star_yellow.png';

import '../css/Rate.css';

const SERVER = 'http://localhost:3001/API/phrases';


export default class Rate extends React.Component {
    constructor(props) {
        super(props)
        let puntuacionTotal = this.props.data.puntuacionTotal * 1;
        let puntuado = this.props.data.puntuado * 1;
        this.state = {
            rating: (puntuacionTotal / puntuado)
        }
        console.log("puntuacion: " + (this.state.rating));
    }

    setRating(rating) {
        if(this.state.disabled)return;
        this.setState({ rating });
        console.log("rating " + rating);

        let rate = {
            puntuacion: rating
        };
        rate = JSON.stringify(rate);
        let fetchUrl = SERVER + "/rate/" + this.props.data.id;
        console.log("fetch to: " + fetchUrl);
        fetch(fetchUrl, {
            method: 'PUT',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: rate
        })
            .then(resp => resp.json())
            .catch(err => console.log("Error Rating: " + err));

        this.setState({ volver: true });

    }

    render() {
        let stars = [];
        let disabled = this.state.disabled ? 'disabled' : '';
        for (let i = 1; i <= 5; i++) {
            stars.push(<img className= {(disabled + " stars")} alt="rating" key={i} 
                        src={i <= this.state.rating ? starYellow : star} onClick={() => {
                                                                                         this.setRating(i)
                                                                                         this.setState({disabled:true})
                        }}></img>)
                        
                        
        }

        return (
            <div className='stars-container'>
                {stars}
            </div>
        );
    }

}
