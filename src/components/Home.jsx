import React, { Component } from 'react';
import { Col } from 'react-bootstrap';

import Mood from './Mood';
import HoroscopeDetail from './HoroscopeDetail';



//IMPORT CSS
import '../css/Home.css';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            musicPlayer: '',
            mood: '',
        }

        this.setMood = this.setMood.bind(this);
        this.setMusicPlayer = this.setMusicPlayer.bind(this);
    }

    setMood(mood) {
        this.setState({ mood: mood })
    }
    //
    setMusicPlayer(activated) {
        this.setState({ musicPlayer: activated });
        console.log("ON HOME - Music player: " + activated)
        if (activated === "on") {

        } else {

        }
    }


    render() {

        let homeVisible = ''
        if (this.props.state.homeVisible === true) {
            homeVisible = 'homeAnimation';
        } else {
            homeVisible = 'homeHide';
        }
 
        return (
            <Col style={{ margin: 'auto' }} md={12} className={homeVisible}>
                <Col md={12}>
                    <HoroscopeDetail signSelected={this.props.state} />
                </Col>
                <Col md={12} style={{ margin: 'auto' }}>
                    {<Mood setMusicPlayer={this.setMusicPlayer} setMood={this.setMood} />}
                </Col>

            </Col>
        );
    }
}

export default Home;