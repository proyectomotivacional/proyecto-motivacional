import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import {Spinner} from 'reactstrap';

//IMPORT CSS

import '../css/HoroscopeDetail.css';

//IMG HOROSCOPE IMPORT


import hor1 from '../img/horoscope/hor1.png';
import hor2 from '../img/horoscope/hor2.png';
import hor3 from '../img/horoscope/hor3.png';
import hor4 from '../img/horoscope/hor4.png';
import hor5 from '../img/horoscope/hor5.png';
import hor6 from '../img/horoscope/hor6.png';
import hor7 from '../img/horoscope/hor7.png';
import hor8 from '../img/horoscope/hor8.png';
import hor9 from '../img/horoscope/hor9.png';
import hor10 from '../img/horoscope/hor10.png';
import hor11 from '../img/horoscope/hor11.png';
import hor12 from '../img/horoscope/hor12.png';
import loading from '../img/loading.png';

let currentHoroscopeImg = '';
let currentHoroscopeRange = '';


class HoroscopeDetail extends Component {


    render() {
        console.log("sunsign " + JSON.stringify(this.props))

        switch (this.props.signSelected.horoscopeData.sunsign) {
            //
            case 'Aquarius':
                currentHoroscopeImg = hor1;
                currentHoroscopeRange = 'January 20 - February 18';
                break;
            //
            case 'Pisces':
                currentHoroscopeImg = hor2;
                currentHoroscopeRange = 'February 19 - March 20';
                break;
            //
            case 'Aries':
                currentHoroscopeImg = hor3;
                currentHoroscopeRange = 'March 21 - April 19';
                break;
            //
            case 'Taurus':
                currentHoroscopeImg = hor4;
                currentHoroscopeRange = 'April 20 - May 20';
                break;
            //
            case 'Gemini':
                currentHoroscopeImg = hor5;
                currentHoroscopeRange = 'May 21 - June 20';
                break;
            //
            case 'Cancer':
                currentHoroscopeImg = hor6;
                currentHoroscopeRange = 'June 21 - July 22';
                break;
            //
            case 'Leo':
                currentHoroscopeImg = hor7;
                currentHoroscopeRange = 'July 23- August 22';
                break;
            //
            case 'Virgo':
                currentHoroscopeImg = hor8;
                currentHoroscopeRange = 'August 23 - September 22';
                break;
            //
            case 'Libra':
                currentHoroscopeImg = hor9;
                currentHoroscopeRange = 'Septembre 23 - October 22';
                break;
            //
            case 'Scorpio':
                currentHoroscopeImg = hor10;
                currentHoroscopeRange = 'October 23 - November 21';
                break;
            //
            case 'Sagittarius':
                currentHoroscopeImg = hor11;
                currentHoroscopeRange = 'November 22 - December 21';
                break;
            //
            case 'Capricorn':
                currentHoroscopeImg = hor12;
                currentHoroscopeRange = 'December 22 - January 19';
                break;

            default:

                break;
        }
        //<img src={loading} className="loadingRotate" alt="Loading" />
        if (!this.props.signSelected.horoscopeData.sunsign) {
            return (
                <Col md={10} sm={12} xs={12} className="horoscope-container-box">
                    <Row>
                        <Col xs={12} md={7} sm={12} lg={3} className="horoscope-container-image">
                            <div style={{ height: '200px', padding:'80px'}}> 
                                <Spinner color="white"  style={{ width: '3rem', height: '3rem'}}/>   
                            </div>
                        </Col>
                    </Row>
                </Col>
            );
        }

        return (
            <Col md={10} sm={12} xs={12} className="horoscope-container-box">
                <Row>
                    <Col xs={12} md={7} sm={12} lg={3} className="horoscope-container-image">
                        <img src={currentHoroscopeImg} alt="Happy" />
                        <div style={{ marginTop: '20px' }} >
                            <h4>{this.props.signSelected.horoscopeData.sunsign}</h4>
                            <p>{currentHoroscopeRange}</p>
                        </div>
                    </Col>
                    <Col xs={12} md={12} sm={12} lg={7} className="horoscope-container-text">
                        <h4>{this.props.signSelected.horoscopeData.date}</h4>
                        <p>{this.props.signSelected.horoscopeData.horoscope}</p>
                    </Col>
                </Row>
            </Col>
        );
    }
}

export default HoroscopeDetail;