import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

//importamos las imagenes
import mood0 from '../img/mood/mood0.png';
import mood1 from '../img/mood/mood1.png';
import mood2 from '../img/mood/mood2.png';
import mood3 from '../img/mood/mood3.png';
import mood4 from '../img/mood/mood4.png';
//IMPORT CSS

import '../css/Mood.css';
class Mood extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedMood: "",
      moods_array: [mood0, mood1, mood2, mood3, mood4]
    }
    this.moodClick = this.moodClick.bind(this);
  }

  moodClick(i) {
    console.log('Mood clicado' + this.state.moods_array[i]);
    this.setState({ selectedMood: i })
    this.props.setMusicPlayer("on");
  }

  render() {
    if (this.state.selectedMood === ""){
      // let moods_array = [mood0, mood1, mood2, mood3, mood4];       
      let moods = this.state.moods_array.map((value, index) => {
        return <Col md={2} xs={1} className="home-mood-box-img" alt="mood" key={index}><img src={value} alt={index} onClick={() => this.moodClick(index)}></img></Col>
      })

      return (
        <Col xs={12} sm={12} md={10} lg={10} className="home-mood-box">
        <h4>How are you feeling today?</h4>
        <Col md={11} lg={7} style={{margin: 'auto', paddingLeft : '0px', left : '-10px'}}>
          <Row>
            {moods}
          </Row>
        </Col>
        </Col>
      )
    } else {
      return (
        <Col xs={12} sm={12} md={10} lg={10} className="home-mood-box">
        <h4>How are you feeling today?</h4>
        <Col md={11} lg={7} style={{margin: 'auto'}}>
          <Row>
            <img src={this.state.moods_array[this.state.selectedMood]} alt="mood" style={{margin:'auto'}}/>
          </Row>
        </Col>
        </Col>
      )
    }

  }





}

export default Mood;