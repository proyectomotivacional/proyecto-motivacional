import React, {Component} from 'react';
import './css/main.css';
import { Col, Container} from 'react-bootstrap';
//Components
import Home from './components/Home';
import HoroscopeCarousel from './components/Horoscope'

import Phrases from './components/Phrases';

const SERVER = 'http://localhost:3001/API';
const MODEL = '/horoscope';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signSelected : '',
      horoscopeVisible : true,
      homeVisible : false,
      horoscopeData: '',
      dataLoaded : false
      
    }
    this.horoscopeClick = this.horoscopeClick.bind(this);
    this.horoscopeData = this.horoscopeData.bind(this);
}


horoscopeClick(value){
  this.setState({
    horoscopeVisible : false,
    homeVisible : true,
    signSelected : value,
  });
  this.horoscopeData(value)
}

horoscopeData(value){

  //let signSelecte= sign;
  //let signSelected = [value];
  let apiUrl = SERVER + MODEL + '/' + value;
  //let apiUrl = `http://horoscope-api.herokuapp.com/horoscope/today/${signSelected}`
  //let fetchURL = "http://horoscope-api.herokuapp.com/horoscope/today/Libra"
  //console.log("Requesting fetchURL from: " + fetchURL);
  console.log("Requesting apiUrl from: " + apiUrl);
  return new Promise((resolve, reject) => {
    fetch(apiUrl)
        .then(results => results.json())
        .then(data => {
          this.setState({horoscopeData : data.data});
          console.log("Horoscope Data: " + data);
        })
        .catch(err => reject([{ error: err }]));
})
}

render(){
  //console.log(this.state.horoscopeData.horoscope);
  return (
    <div className="background-body">
        <Col className='app-banner container-fluid'>
          <Phrases />
        </Col>
      <Container>
        <Col>
          <HoroscopeCarousel horoscopeClick={this.horoscopeClick} state={this.state}/>
        </Col>
        <Col>
        <Home state={this.state}/>
        </Col>
      </Container>
    </div>
  )};
}

export default App;
